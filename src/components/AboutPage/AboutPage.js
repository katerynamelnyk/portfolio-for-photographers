import React, { useEffect, useRef } from "react";

import styles from './AboutPage.module.css';
import CircleType from 'circletype';
import Circles from "../UI/Circles/Circles";
import CircledText from "../UI/CircledText/CircledText";

const AboutPage = props => {

    const circleInstance2 = useRef();

    useEffect(() => {
        new CircleType(circleInstance2.current).radius(40).forceWidth(60);
    }, []);


    return (
        <React.Fragment>
            <div className={styles.wrapper}>
                <p className={styles.text}>ABOUT</p>

                <div className={styles.flexContainer}>
                    <div className={styles.block}>
                        <div className={`${styles.wrapperForImage} ${styles.wrapperForImageOver}`}>
                            <img className={styles.photo} src={require('./Kate.jpg')} alt="Kate"></img>
                        </div>
                        <Circles nameLeft='KATE' className={styles.circleLeft}></Circles>
                    </div>
                    <div className={styles.textBlock}>
                        <div className={styles.wrapperForText}>
                            <p className={styles.mainText}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus mollis massa sed lectus mattis, sed euismod leo finibus. Integer dignissim eros at tortor tincidunt, eu consectetur turpis aliquet. Donec in malesuada nulla, at mattis lorem. Ut consectetur pretium sapien id ultricies. Pellentesque nec rhoncus leo. Proin mollis euismod enim, id blandit velit dignissim sit amet. Etiam commodo finibus tristique. Mauris vehicula efficitur nibh non vehicula. Cras non vestibulum est, nec sagittis ipsum. Nulla commodo euismod vestibulum. In hac habitasse platea dictumst.
                            </p>
                            <CircledText className={styles.circledText} text='A&K Photographers' radius='90' forceWidth='40'></CircledText>
                        </div>
                        <div className={styles.wrapperForButton}>
                            <p ref={circleInstance2} 
                            className={styles.buttonText}>&nbsp;READ MORE * READ MORE *</p>
                            <button type="button"></button>
                        </div>

                    </div>
                    <div className={styles.block}>
                        <div className={styles.wrapperForImage}>
                            <img className={styles.photo} src={require('./Anna.jpg')} alt="Anna"></img>
                        </div>

                        <Circles nameRight='ANN' className={styles.circleRight}></Circles>
                    </div>

                </div>
            </div>
        </React.Fragment>
    )
}


export default AboutPage;