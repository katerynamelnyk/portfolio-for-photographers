import React from "react";

import './Circles.css';

const Circles = props => {

    const name = 'circlesWrapper ' + props.className;

    return (
        <div className={name}>
            <div className="circle circle1"></div>
            <div className="circle circle2">
                <p className='textSmall textSmallRotated'>{props.nameRight}</p>
            </div>
            <div className="circle circle3">
                <p className='textSmall'>{props.nameLeft}</p>
            </div>
            <div className="circle circle4"></div>
            <div className="circle circle5"></div>
        </div>
    )
}

export default Circles;