import React, { useRef, useEffect } from "react";
import CircleType from 'circletype';
import './CircledText.css';

const CircledText = props => {

    const circleInstance = useRef();

    useEffect(() => {
        new CircleType(circleInstance.current).radius(+props.radius).forceWidth(+props.forceWidth);
    });


    let name = 'animatedText ' + props.className;

    if (props.text === 'A&K Photographers') {
        name = 'circledText ' + props.className;
    }

    return <p className={name} ref={circleInstance}>{props.text}</p>

}

export default CircledText;