import React from "react";
import styles from './GetInTouch.module.css';

import CircledText from "../UI/CircledText/CircledText";
import Circles from '../UI/Circles/Circles'


const GetInTouch = props => {

    return (
        <React.Fragment>
            <div className={styles.wrapper}>
                <div className={styles.textWrapper}>
                    <p className={styles.text}>GET IN TOUCH</p>
                    <CircledText
                        text='available worldwide'
                        radius={70} forceWidth={10}></CircledText>
                </div>
                <Circles className={styles.circles}></Circles>
                <div className={styles.mainContent}>
                    <div className={styles.wrapperForImage}>
                        <img src={require('./spring.jpg')} alt='spring'></img>
                    </div>
                    <form>
                        <div className={styles.inputItem}>
                            <label for="firstName">FIRST NAME: *</label>
                            <input placeholder="FIRST NAME" type="text" id="firstName"></input>
                        </div>
                        <div className={styles.inputItem}>
                            <label for="lastName">LAST NAME: *</label>
                            <input placeholder="LAST NAME" type="text" id="lastName"></input>
                        </div>
                        <div className={styles.inputItem}>
                            <label for="email">E-MAIL: *</label>
                            <input placeholder="E-MAIL ADDRESS" type="email" id="email"></input>
                        </div>
                        <div className={styles.inputItem}>
                            <label for="serves">HOW CAN WE BEST SERVE YOU? *</label>
                            <select id="serves">
                                <option value="phone" disabled selected>SELECT</option>
                                <option value="phone">phone</option>
                                <option value="email">email</option>
                                <option value="telegram">telegram</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </React.Fragment>
    )
}

export default GetInTouch;