import React from "react";

import styles from './ThirdPage.module.css';

const ThirdPage = props => {
    return (
        <React.Fragment>
            <div className={styles.wrapper}>
                <p className={styles.text}>"Photography helps people to see"</p>
            </div>
        </React.Fragment>
    )
}

export default ThirdPage;