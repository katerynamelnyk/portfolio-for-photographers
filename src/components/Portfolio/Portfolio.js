import React from 'react';
import GetInTouch from '../GetInTouch/GetInTouch';
import CircledText from '../UI/CircledText/CircledText';
import Circles from '../UI/Circles/Circles';

import styles from './Portfolio.module.css';

const Portfolio = props => {
    return (
        <React.Fragment>
            <div className={styles.wrapper}>
                <div className={styles.textWrapper}>
                    <p className={styles.text}>PORTFOLIO</p>
                    <CircledText text='A&K Photographers' radius='90' forceWidth='40'></CircledText>
                </div>
                <Circles className={styles.circlesRight}></Circles>
                <Circles className={styles.circlesLeft}></Circles>
                <div className={styles.blockWithPhotos}>
                    <div>
                        <div className={styles.wrapperForImage}>
                            <img className={styles.photo} src={require('./wedding.jpg')} alt="wedding"></img>
                        </div>
                        <div className={styles.wrapperForButton}>
                            <button>WEDDING DAY</button>
                        </div>
                    </div>
                    <div>
                        <div className={styles.wrapperForImage}>
                            <img className={styles.photo} src={require('./individual.jpg')} alt="individual"></img>
                        </div>
                        <div className={styles.wrapperForButton}>
                            <button>INDIVIDUAL PHOTOSHOOT</button>
                        </div>
                    </div>
                    <div>
                        <div className={styles.wrapperForImage}>
                            <img className={styles.photo} src={require('./kids.jpg')} alt="kids"></img>
                        </div>
                        <div className={styles.wrapperForButton}>
                            <button>KIDS PHOTO</button>
                        </div>
                    </div>
                    <div>
                        <div className={styles.wrapperForImage}>
                            <img className={styles.photo} src={require('./family.jpg')} alt="family"></img>
                        </div>
                        <div className={styles.wrapperForButton}>
                            <button>FAMILY PHOTOSESSION</button>
                        </div>
                    </div>
                    <div>
                        <div className={styles.wrapperForImage}>
                            <img className={styles.photo} src={require('./lovestory.jpg')} alt="lovestory"></img>
                        </div>
                        <div className={styles.wrapperForButton}>
                            <button>LOVE STORY</button>
                        </div>
                    </div>
                    <div>
                        <div className={styles.wrapperForImage}>
                            <img className={styles.photo} src={require('./pregnant.jpg')} alt="pregnant"></img>
                        </div>
                        <div className={styles.wrapperForButton}>
                            <button>PREGNANT</button>
                        </div>
                    </div>
                </div>
                <GetInTouch></GetInTouch>
            </div>

        </React.Fragment>
    )
}

export default Portfolio;