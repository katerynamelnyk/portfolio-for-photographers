import React from "react";
import styles from './Link.module.css';

const Link = props => {
    return (
        <a href = {props.url} className={`${styles.link} 
        ${props.isActive ? styles.active : ''} 
        ${props.isSocialMedia ? styles.socialMedia : ''}`}>{props.children}</a>
    )
}

export default Link;