import React from "react";
import styles from './Links.module.css';
import Link from "./Link";

const Links = props => {
    return (
        <nav className={styles.navBlock}>
            {props.data.map(item =>
                <Link
                    url={item.url}
                    isSocialMedia={props.isSocialMedia}
                    isActive={item.active}>{item.name}</Link>)}
        </nav>
    )
}

export default Links;