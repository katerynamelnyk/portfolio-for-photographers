import React, { useEffect, useRef } from 'react';
import Links from './Links/Links';
import styles from './MainPage.module.css';
import CircleType from 'circletype';

const MainPage = () => {

    const mainNav = [
        { name: 'ABOUT', active: false },
        { name: 'PORTFOLIO', active: false },
        { name: 'PRICE', active: false },
        { name: 'CONTACT', active: false },
        { name: 'BLOG', active: false },
    ]

    const languageNav = [
        { name: 'EN', active: true },
        { name: 'RU', active: false },
        { name: 'PL', active: false },
    ]

    const socialMediaNav = [
        { name: 'Instagram', active: false, url: 'https://www.instagram.com/mlnk_photo' },
        { name: 'Behance', active: false, url: 'https://www.behance.net' },
        { name: 'Pinterest', active: false, url: 'https://www.pinterest.com' },
    ]

    const circleInstance = useRef();

    useEffect(() => {
        new CircleType(circleInstance.current).radius(250).forceWidth(40);
    }, []);

    return (
        <React.Fragment>
            <div className={styles.mainPageWrapper}>
                <div className={styles.linksWrapper}>
                    <Links data={mainNav}></Links>
                    <Links data={languageNav}></Links>
                </div>
                <div className={styles.mainText}>
                    <h1>ANNA&KATE MELNYK PHOTOGRAPHERS
                        <span className={styles.animatedText} ref={circleInstance}>available worldwide</span>
                    </h1>
                </div>
                <Links data={socialMediaNav} isSocialMedia={true}></Links>
            </div>
        </React.Fragment>
    )
}

export default MainPage;