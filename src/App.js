import React from 'react';
import './App.css';
import AboutPage from './components/AboutPage/AboutPage';
import MainPage from './components/MainPage/MainPage';
import Portfolio from './components/Portfolio/Portfolio';
import ThirdPage from './components/ThirdPage/ThirdPage';


function App() {
  return (
    <React.Fragment>
      <MainPage></MainPage>
      <AboutPage></AboutPage>
      <ThirdPage></ThirdPage>
      <Portfolio></Portfolio>
    </React.Fragment>
  )
}

export default App;
